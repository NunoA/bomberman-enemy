#include "Cell.h"
#include <time.h>
#include <stdlib.h>
#include <tchar.h>
#include <windows.h>
#include <stdio.h>


#define TABLE_SIZE 15
#define TAM 15
#define NUMBER_OF_INDESTRUCTIBLE_BLOCKS 20
#define NUMBER_OF_BLOCKS 50
#define NUMBBER_OF_PLAYERS 2
#define NUMBBER_OF_ENEMIES 2


	
typedef struct {
	Cell table[TABLE_SIZE][TABLE_SIZE];
	Player players[NUMBBER_OF_PLAYERS];
}Table;

Table createTable(Table t);
void placeObject(int numberOfBlocks, ObjectTypes type, Table *t);
void tableToString(Table *t);
Cell findPlayer(ObjectTypes player, Table table);