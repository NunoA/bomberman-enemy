
#include <time.h>
#include <process.h>
#include <conio.h>
#include <windows.h>
#include <io.h>
#include <conio.h>
#include <fcntl.h>
#include <stdio.h>
#include <tchar.h>
#include <aclapi.h>
#include <strsafe.h>
#include "Table.h"
#include "Mensagem.h"
using namespace std;
#pragma comment(lib, "user32.lib")

#define PIPE_COM TEXT("\\\\.\\pipe\\comunica")
#define TABLE_MUTEX_NAME TEXT("TableMutex")

HANDLE hPipeComunica;
HANDLE hPipeBroadcast;
DWORD WINAPI escreverPipe(LPVOID param);
#define PIPE_BROAD TEXT("\\\\.\\pipe\\broadcast")

TCHAR nomeMem[] = TEXT("MapaPartilhado");

Table *ptrTabMem;//ponteiro mem partilhada
DWORD WINAPI lerTabuleiro(LPVOID param);

int enemyType;
int enemyNumber;

Mensagem enemySms;
HANDLE hMutex;




// Recebe por argumento tipo de inimigo
int _tmain(int argc, LPCTSTR argv[]){

#ifdef UNICODE 
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
	_setmode(_fileno(stderr), _O_WTEXT);
#endif

	//create mutex
	hMutex = CreateMutex(NULL, FALSE, TABLE_MUTEX_NAME);

	HANDLE hMapFile;// handle memoria
	HANDLE hThreadJogar = NULL;// handle da thread
	/*
	_tprintf(TEXT("inimigo %s\n\n\n\n"),argv[1]);

	for (int sa = 0; sa < argc; sa++){
		_tprintf(TEXT("\ninimigo %d - %s"), sa, argv[sa]);
	}*/
	if (_tccmp(TEXT("1"), argv[1])==0){
		enemyNumber = 1;
	}
	else{
		enemyNumber = 0;
	}
	enemyType = enemyNumber;

	if (enemyType == 1)// copiar para o sitio certo
		strcpy_s(enemySms.user, "hacker");
	else
		strcpy_s(enemySms.user, "donkey");

	_tprintf(TEXT("Enemy number: %d\n"),enemyNumber);

	_tprintf(TEXT(" Memoria Partilhada \n"));

	hMapFile = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, nomeMem);

	ptrTabMem = (Table *)MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(Table));// V� o tabuleiro

	hThreadJogar = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)lerTabuleiro, (LPVOID)ptrTabMem, 0, NULL);// Thread para accoes

	// Ligar inimigos aos pipes
	if (!WaitNamedPipe(PIPE_COM, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (WaitNamedPipe)\n"), PIPE_COM);
		exit(-1);
	}

	hPipeComunica = CreateFile(PIPE_COM, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hPipeComunica == NULL) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (CreateFile)\n"), PIPE_COM);
		exit(-1);
	}

	WaitForSingleObject(hThreadJogar, INFINITE);// Aqui ou antes pipe

	return 0;
}


/*
* Funcao para imprimir tabuleiro
*/
void imprime(){
	//system("cls");
	Table t = *ptrTabMem;
	_tprintf(TEXT("\n\n"));
	for (int y = 0; y < TABLE_SIZE; y++){
		_tprintf(TEXT("\t\t"));
		for (int x = 0; x < TABLE_SIZE; x++){
			switch (t.table[x][y].types[0]){

			case INDESTRUCTIBLE_BLOCK:
				_tprintf(TEXT("##"));
				break;
			case BLOCK:
				_tprintf(TEXT("$$"));
				break;
			case PLAYER:
				_tprintf(TEXT("P%d"), t.table[x][y].playerNumber);
				break;
			case ENEMY:
				_tprintf(TEXT("E%d"), t.table[x][y].playerNumber);
				break;
			case BLANK:
				switch (t.table[x][y].types[1]){
				case BOMB:
					_tprintf(TEXT("**"));
					break;
				case EXTRALIFE:
					_tprintf(TEXT("EL"));
					break;
				case ARTFACT:
					_tprintf(TEXT("AF"));
					break;
				case EXTRAPOWER:
					_tprintf(TEXT("EP"));
					break;
				case NUKEBOMB:
					_tprintf(TEXT("NB"));
					break;
				default:
					_tprintf(TEXT("__"));
					break;
				}

				break;
			default:
				break;
			}
		}
		_tprintf(TEXT("\n"));
	}
	_tprintf(TEXT("\n\n\n"));
}

/*
* � o Enemy 0
* Funcao do donkey
*/
void donkey(){
	//Verificar que accao vai realizar o donkey
	int accao = rand() % 5;
	switch (accao)
	{
	case 0: enemySms.action = UP;
		break;
	case 1:	enemySms.action = DOWN;
		break;
	case 2:	enemySms.action = RIGHT;
		break;
	case 3:	enemySms.action = LEFT;
		break;
	case 4:	enemySms.action = PUTBOMB;
		break;
	default:
		break;
	}
}
/*
* � o Enemy 1
* Funcao do Hacker
*/
void hacker(){// enemy 1 = HACKER
	//Verif
	int	 atacar, player;

	int bomba = rand() % 10;
	if (bomba > 6)
		enemySms.action = PUTBOMB;// mete bomba

	else{ // SENAO MOVER	
		int	 atacar = rand() % 10;// verifica qual ataca

		if (atacar > 5){
			player = 1;
			if (ptrTabMem->players[player].x == -1 && ptrTabMem->players[player].y == -1){
				player = 0;
			}
		}
		else{
			player = 0;
			if (ptrTabMem->players[player].x == -1 && ptrTabMem->players[player].y == -1){
				player = 1;
			}
		}

		int playerToChase = rand()%2;
		

		//VERIFICAR PARA ONDE MOVE
		if (ptrTabMem->players[player].x > ptrTabMem->players[playerToChase].x)// esta a direita
			enemySms.action = RIGHT;
		else if (ptrTabMem->players[player].x < ptrTabMem->players[playerToChase].x)// esta a esquerda
			enemySms.action = LEFT;
		else if (ptrTabMem->players[player].y < ptrTabMem->players[playerToChase].y)// esta a esquerda
			enemySms.action = DOWN;
		else
			enemySms.action = UP;

	}
}

/*
*Check if this enemy is dead
*/
bool verifyIfDead(){
	int i = 0;
	while (ptrTabMem->players[i].type != ENEMY){
		i++;
	}
	i += enemyNumber;
	if (ptrTabMem->players[i].health <= 0){
		return true;
	}

	return false;
}

DWORD WINAPI lerTabuleiro(LPVOID param){
	// ve tabuleiro de 1 em 1 segundo
	DWORD dwWaitResult;
	Sleep(4000);
	while (1){
		Sleep(1000);
		if (verifyIfDead()){
			_tprintf(TEXT("MORRI"));
			DisconnectNamedPipe(hPipeComunica);
			exit(0);
		}

		dwWaitResult = WaitForSingleObject(
			hMutex,    // handle to mutex
			INFINITE);  // no time-out interval
		switch (dwWaitResult)
		{
		case WAIT_OBJECT_0:
			// chama bot Donkey ou SMART
			if (!enemyNumber == 1){//Donkey
				donkey();
			}
			else{//SMART
				hacker();
			}

			ReleaseMutex(hMutex);
			break;
		case WAIT_ABANDONED:
		default:
			_tprintf(TEXT("Mutex Error"));
			return dwWaitResult;
		}

		escreverPipe(hPipeComunica);
		_tprintf(TEXT("Action: "));
		switch (enemySms.action)
		{
		case UP:
			_tprintf(TEXT("UP\n"));
			break;
		case DOWN:
			_tprintf(TEXT("DOWN\n"));
			break;
		case LEFT:
			_tprintf(TEXT("LEFT\n"));
			break;
		case RIGHT:
			_tprintf(TEXT("RIGHT\n"));
			break;
		case PUTBOMB:
			_tprintf(TEXT("PUTBOMB\n"));
			break;
		default:
			break;
		}
		imprime();
	}

}
/*
* ESCREVER PIPE NORMAL
*@param Handle pipe
*/
DWORD WINAPI escreverPipe(LPVOID param){//
	DWORD n;
	int i;
	TCHAR buf[256];

	n = sizeof(Mensagem);

	WriteFile(hPipeComunica, &enemySms, sizeof(Mensagem), &n, NULL);
	//	bufa[n / sizeof(TCHAR)] = '\0';
	//_tprintf(TEXT("[CLIENTE] Enviei %s bytes ao serv... \n"), bufa);

	return 0;
}