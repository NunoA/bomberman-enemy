/*
*this class will handle with the table itself
*/
#include "Table.h"

/*
Table createTable(Table t){
	srand(time(NULL));
	int r = rand();


	for (int x = 0; x < TAM; x++)
		for (int y = 0; y < TAM; y++){
			t.table[x][y].x = x;
			t.table[x][y].y = y;
			t.table[x][y].health = 0;
			t.table[x][y].types[0] = BLANK;

		}
	t.table[0][0].types[0] = PLAYER1;
	t.table[TAM - 1][TAM - 1].types[0] = PLAYER2;

	placeObject(NUMBER_OF_INDESTRUCTIBLE_BLOCKS, INDESTRUCTIBLE_BLOCK, &t);
	placeObject(NUMBER_OF_BLOCKS, BLOCK, &t);

	return t;
}
	

void placeObject(int numberOfBlocks, enum ObjectTypes type, Table *t){

	while (numberOfBlocks > 0){
		int x = rand() % TAM;
		int y = rand() % TAM;

		if (t->table[x][y].types[0] == BLANK){
			t->table[x][y].types[0] = type;
			t->table[x][y].health = -1;
			numberOfBlocks--;
		}
	}
}
*/

void tableToString(Table t){
	//system("cls");

	_tprintf(TEXT("\n\n"));
	for (int y = 0; y < TABLE_SIZE; y++){
		_tprintf(TEXT("\t\t"));
		for (int x = 0; x < TABLE_SIZE; x++){
			switch (t.table[x][y].types[0]){

			case INDESTRUCTIBLE_BLOCK:
				_tprintf(TEXT("##"));
				break;
			case BLOCK:
				_tprintf(TEXT("$$"));
				break;
			case PLAYER:
				_tprintf(TEXT("P%d"), t.table[x][y].playerNumber);
				break;
			case ENEMY:
				_tprintf(TEXT("E%d"), t.table[x][y].playerNumber);
				break;
			case BLANK:
				switch (t.table[x][y].types[1]){
				case BOMB:
					_tprintf(TEXT("**"));
					break;
				case EXTRALIFE:
					_tprintf(TEXT("EL"));
					break;
				case ARTFACT:
					_tprintf(TEXT("AF"));
					break;
				case EXTRAPOWER:
					_tprintf(TEXT("EP"));
					break;
				case NUKEBOMB:
					_tprintf(TEXT("NB"));
					break;
				default:
					_tprintf(TEXT("__"));
					break;
				}

				break;
			default:
				break;
			}
		}
		_tprintf(TEXT("\n"));
	}
	_tprintf(TEXT("\n\n\n"));
}

/*
Cell findPlayer(ObjectTypes player, Table table){
	for (int i = 0; i < TAM; i++){
		for (int j = 0; j < TAM; j++){
			if (table.table[i][j].types[0] == player){
				return table.table[i][j];
			}
		}
	}
}*/